"use strict";

/**
 * Example
 */

// First, we import the parse function
let parse = require('./..');

/*
 * In many programming puzzles, data is presented
 * in form of a structured text file.
 * This module aims to simplify the parsing step.
 * The data might look like this:
 * (assume that the file was already read to a list)
 */

let data = [
    // Two amounts, 'a' and 'b', indicating how many elements there
    // are in the lists 'A' and 'B'.
    2, 3,
    // Then two lines with 3 elements each, which are to be
    // stored in the list 'A'.
    1, 2, 4,
    5, 6, 8,
    // Finally three lines with 2 elements each for list 'B'.
    1, 2,
    3, 4,
    5, 6
];

/*
 * We want the data to look like this
 *
 *  {
 *      a: 2, b: 3,
 *      A: [
 *          { first: 1, second: 2, third: 4 },
 *          { first: 5, second: 6, third: 8 }
 *      ],
 *      B: [
 *          { first: 1, second: 2 },
 *          { first: 3, second: 4 },
 *          { first: 5, second: 6 }
 *      ]
 *  }
 *
 * So we define the structure we want the data to have
 */

// The structure is a list
let structure = [
    // Variable names starting with '!' are stored to a
    // dictionary for re-use.
    "!a", "!b",
    // Then we define the list 'A'
    {
        // We define a name for the list
        name: "A",
        // We re-use 'a' as quantity of elements in 'A'
        qty: "a",
        // And we define the inner structure for elements
        // in 'A'.
        structure: [
            "first", "second", "third"
        ]
    },
    // Same for 'B'
    {
        name: "B",
        qty: "b",
        structure: [
            "first", "second"
        ]
    }
];

// Now we can parse the data.
let result = parse(structure, data);
console.log("First example:\n");
console.log(result);

/*
 * Got it? Then now a real-world example, from
 * the Google Hashcode qualification round 2017
 */

let data2 = [
    5, 2, 4, 3, 100,
    50, 50, 80, 30, 110,
    1000, 3,
    0, 100,
    2, 200,
    1, 300,
    500, 0,
    3, 0, 1500,
    0, 1, 1000,
    4, 0, 500,
    1, 0, 1000
];

let structure2 = [
    '!videos', '!endpoints', '!requests', 'caches', 'size',
    {
        name: 'Videos',
        qty: 'videos',
        structure: [ 'size' ]
    },
    {
        name: 'Endpoints',
        qty: 'endpoints',
        structure: [
            'latency', '!caches',
            {
                name: 'Caches',
                qty: 'caches',
                structure: [
                    'id', 'latency'
                ]
            }
        ]
    },
    {
        name: 'Requests',
        qty: 'requests',
        structure: [
            'video', 'endpoint', 'weight'
        ]
    }
];

let result2 = parse(structure2, data2);
console.log("\nSecond example:\n");
console.log(result2);