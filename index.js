'use strict'

/**
 * Parser for Hashcode problems
 */

const _ = require('lodash')

// Global dictionary for values
let dict

/**
 * Creates an iterator for a given array a.
 */
function * arrayIterator (array) {
  let i = 0
  while (i < array.length) yield array[i++]
}

// Define a global iterator for the input data
let iter
const next = () => iter.next().value

/**
 * Creates structured data from input.
 * @param {*} structure Structure of the input data.
 * @param {*} input   Input data.
 */
module.exports = function parseData (structure, input) {
  // Create iterator
  iter = arrayIterator(input)

  // Create dictionary
  dict = {}

  // Parse data
  return parseRec(structure)
}

/**
 * Recursively parse data.
 * @param {*} structure (Sub)Structure for current data chunk.
 */
function parseRec (structure) {
  const size = structure.length
  const result = {}

  for (let i = 0; i < size; i++) {
    let el = structure[i]

    // current element is a string,
    // store as variable
    if (_.isString(el)) {
      const value = next()

      // Names beginning with '!' are stored to a dictionary
      if (el[0] === '!') {
        el = el.substr(1)
        dict[el] = value
      }

      result[el] = value
    } else { // Current element is a substructure
      const list = []

      // create list of elements from substructure
      for (let j = 0; j < dict[el.qty]; j++) {
        list.push(parseRec(el.structure))
      }
      result[el.name] = list
    }
  }
  return result
}
