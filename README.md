# PippoJS

Parsing interface for programming puzzles and other stuff.

## Usage

Install as dependency using

```text
yarn add https://gitlab.com/rscolati/pippojs.git
```
and then import and use

```js
let parse = require('pippojs');
let res = parse(structure, data);
```

TODO: details, for now check out the documented example [example.js](examples/example.js).