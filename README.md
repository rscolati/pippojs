# PippoJS

The **P**arsing **I**nterface for **P**rogramming **P**uzzles and **O**ther stuff.

## Usage

Install as dependency using

```text
yarn add https://bitbucket.org/rscolati/pippojs.git
```
and then import and use

```js
let parse = require('pippojs');
let res = parse(structure, data);
```

TODO: details, for now check out the documented example [example.js](examples/example.js).