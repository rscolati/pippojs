"use strict";

/**
 * Parser Test
 */

const chai = require('chai');
const fs = require('fs');

const expect = chai.expect;

let parse = require('./..'),
    testData = fs.readFileSync('examples/testdata.json', 'utf8');

const td = JSON.parse(testData);

let input,
    data,
    structure;

describe('Parser test', function() {

    // Use pizza problem test data
    describe('Pizza problem', function() {
        before(function() {
            input = td.pizza_in;
            data = td.pizza_out;
            
            structure = [
                '!noRows', '!noCols', 'minIngredients', 'maxCells',
                {
                    name: 'rows',
                    qty: 'noRows',
                    structure: [ 'row' ]
                }
            ];
        });

        it('should parse correctly', function() {
            expect(data).to.be.deep.equal(parse(structure, input));
        });
    });

    // With videos problem test data
    describe('Videos problem', function() {

        before(function() {
            input = td.videos_in;
            data = td.videos_out;
            
            structure = [
                '!v', '!e', '!r', '!c', '!s',
                {
                    name: 'videos',
                    qty: 'v',
                    structure: [ 'size' ]
                },
                {
                    name: 'endpoints',
                    qty: 'e',
                    structure: [
                        'latency', '!cc',
                        {
                            name: 'caches',
                            qty: 'cc',
                            structure: [
                                'id', 'latency'
                            ]
                        }
                    ]
                },
                {
                    name: 'requests',
                    qty: 'r',
                    structure: [
                        'video', 'endpoint', 'weight'
                    ]
                }
            ];
        });

        it('should parse correctly', function() {
            expect(data).to.be.deep.equal(parse(structure, input));
        });
    });

    // With drones problem test data
    describe('Drones problem', function() {

        before(function() {
            input = td.drones_in;
            data = td.drones_out;

            structure = [
                'rows', 'cols', 'drones', 'turns', 'maxLoad',
                '!noProducts',
                {
                    name: 'products',
                    qty: 'noProducts',
                    structure: [ 'weight' ]
                },
                '!noWarehouses',
                {
                    name: 'warehouses',
                    qty: 'noWarehouses',
                    structure: [
                        'x', 'y',
                        {
                            name: 'storedProducts',
                            qty: 'noProducts',
                            structure: [
                                'qty'
                            ]
                        }
                    ]
                },
                '!noOrders',
                {
                    name: 'orders',
                    qty: 'noOrders',
                    structure: [
                        'x', 'y',
                        '!noItems',
                        {
                            name: 'items',
                            qty: 'noItems',
                            structure: [
                                'productType'
                            ]
                        }
                    ]
                }
            ];
        });

        it('should parse correctly', function() {
            expect(data).to.be.deep.equal(parse(structure, input));
        });
    });
});